# Bird of Prey
Challenge 3 - Games Programming.
Created as part of the Launchpad technical tests.
Created in Unity 2019.2.8f1 with Microsoft Visual Studio 2019.
### Instructions
Collect at least one coin before reaching the finish line.

### Controls
The player can be controlled with the keyboard.

| Control |Key |
| --------|---------|
| Speed | UpArrow/DownArrow |
| Elevation | Space/Shift |
| Yaw | A/D |
| Pitch | W/S |
| Roll | Q/E |
| Exit Game | Escape |

### Assets
All the assets contained within the 'Assets/Imported Assets' folder were provided by the Unity asset store or Freesound.org:

 - [Potions, Coin And Box of Pandora Pack](https://assetstore.unity.com/packages/3d/props/potions-coin-and-box-of-pandora-pack-71778)
 - [Floor materials pack v.1](https://assetstore.unity.com/packages/2d/textures-materials/floors/floor-materials-pack-v-1-140435)
 - [City Street Skyboxes Vol. 1](https://assetstore.unity.com/packages/2d/textures-materials/sky/city-street-skyboxes-vol-1-157401)
 - [Background Alien Skyscrapers with Transit System - Free](https://assetstore.unity.com/packages/3d/environments/sci-fi/background-alien-skyscrapers-with-transit-system-free-9832)
 - [Egypt Pack - Eagle](https://assetstore.unity.com/packages/3d/characters/animals/birds/egypt-pack-eagle-140079)
 - [Four Evil Dragons Pack HP](https://assetstore.unity.com/packages/3d/characters/creatures/four-evil-dragons-pack-hp-79398)
 - [wings flapping.wav](https://freesound.org/people/tommy_mooney/sounds/386706)
 - [Dragon_Wing.wav](https://freesound.org/people/Brsjak/sounds/483095)

Created by Luke Dixon
