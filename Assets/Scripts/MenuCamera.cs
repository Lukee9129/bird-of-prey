﻿using UnityEngine;

public class MenuCamera : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        // Slowly rotate in menu
        transform.RotateAround(transform.position, Vector3.up, 10 * Time.deltaTime);
    }
}
