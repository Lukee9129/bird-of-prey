﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    public static float Volume = 0.5f;
    public Texture FadeTexture;
    private int fadeDirection = 0;
    private float fadeAlpha = 0f;
    private bool fading = false;
    private Action action = null;
    private bool hasConcluded = false;

    void Awake()
    {
        SceneManager.sceneLoaded += OnSceneLoad; // Main scene should be faded in once loaded

        // If this is the main (game) scene
        if (SceneManager.GetActiveScene().buildIndex == 1)
        {
            // Set the volume of all the game objects which have an audio source.
            GameObject[] coins = GameObject.FindGameObjectsWithTag("Coin");
            foreach (GameObject coin in coins)
            {
                SetAudioSourceVolume(coin);
            }

            GameObject player = GameObject.FindGameObjectWithTag("Player");
            SetAudioSourceVolume(player);
            GameObject dragon = GameObject.FindGameObjectWithTag("Dragon");
            SetAudioSourceVolume(dragon);
        }
    }

    public void StartGame()
    {
        // Play/retry button pressed
        FadeOut();

        action = () =>
        {
            SceneManager.LoadScene(1); // Load the next scene
        }; // This action will be run once fading has completed.
    }

    public void GameOver()
    {
        if (hasConcluded) return; // Prevent GameOver triggering after GameWin (getting caught by dragon after going through finish line)
        hasConcluded = true;

        FadeOut();
        action = () =>
        {
            SceneManager.LoadScene(2);
        };
    }

    public void GameWin()
    {
        if (hasConcluded) return;
        hasConcluded = true;

        FadeOut();
        action = () =>
        {
            SceneManager.LoadScene(3);
        };
    }

    public void SetVolume(float vol)
    {
        // Set volume setting
        SceneController.Volume = vol;
    }

    private void SetAudioSourceVolume(GameObject go)
    {
        // Set the volume of the given gameobject's audio source component
        go.GetComponent<AudioSource>().volume = SceneController.Volume;
    }

    private void OnSceneLoad(Scene scene, LoadSceneMode mode)
    {
        SceneManager.sceneLoaded -= OnSceneLoad; // Stop this method running an additional time per scene load.
        FadeIn();
    }

    // Methods for smooth transitions between scenes (fade to and from black)
    private void FadeIn()
    {
        fadeDirection = -1;
        fadeAlpha = 1f;
        fading = true;
    }

    private void FadeOut()
    {
        fadeDirection = 1;
        fadeAlpha = 0f;
        fading = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }

    void OnGUI()
    {
        if (fading)
        {
            // Change alpha over time for fade effect
            fadeAlpha += fadeDirection * 0.5f * Time.deltaTime;
            if (fadeAlpha >= 1 || fadeAlpha <= 0)
            {
                fading = false;
                action?.Invoke(); // Fading complete, change scene if requested.
            }
            fadeAlpha = Mathf.Clamp(fadeAlpha, 0, 1);
        }

        Color temp = GUI.color; // Save previous colour
        // Draw black texture with changing alpha on top of everything else.
        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, fadeAlpha);
        GUI.depth = -999;
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), FadeTexture);
        GUI.color = temp; // Return to previous colour
    }
}
