﻿using UnityEngine;

public class CoinController : MonoBehaviour
{
    private AudioSource sfx;
    private ScoreCounter scoreCounter;
    private bool isCollected = false;

    // Start is called before the first frame update
    void Start()
    {
        scoreCounter = GameObject.FindGameObjectWithTag("Score").GetComponent<ScoreCounter>();
        sfx = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider col)
    {
        if (isCollected) return; // If this coin hasn't already been collected

        if (col.gameObject.CompareTag("Player")) // If colliding with the player
        {
            isCollected = true;
            scoreCounter.IncrementScore(); // Then increment score by 1
            sfx.Play(); // Play sound effect
            // Hide the coin
            Renderer[] renderers = GetComponentsInChildren<Renderer>();
            foreach (Renderer r in renderers)
                r.enabled = false;
            // Destroy it after 2 seconds, allowing the sound effect to play.
            Destroy(gameObject, 1);
        }
    }

    void Update()
    {
        // Constantly rotate
        transform.Rotate(Vector3.up, 50 * Time.deltaTime);
    }
}
