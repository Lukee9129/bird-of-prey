﻿using UnityEngine;
using UnityEngine.UI;

public class DisplayScore : MonoBehaviour
{
    private Text text;
    // Start is called before the first frame update
    void Start()
    {
        // Display final score.
        text = GetComponent<Text>();
        text.text = $"You scored: {ScoreCounter.Score}/{ScoreCounter.MaxScore}";
    }
}
