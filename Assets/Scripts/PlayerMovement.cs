﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody rb;
    private float angularSpeed = 0.2f;
    private float linearSpeed = 20f;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        // Set drag and angular drag (physics)
        rb.drag = 1f;
        rb.angularDrag = 2f;
    }

    // Physics updates are done here
    void FixedUpdate()
    {
        // Get player input for elevation, speed, yaw, pitch and roll
        float elevation = Input.GetAxis("Elevation");
        float speed = Input.GetAxis("Speed");
        float yaw = Input.GetAxis("Yaw");
        float pitch = Input.GetAxis("Pitch");
        float roll = Input.GetAxis("Roll");
        // Use pitch, yaw and roll for torque
        Vector3 torque = new Vector3(pitch, yaw, -roll) * angularSpeed;
        // Use elevation and speed for force
        Vector3 force = new Vector3(0, elevation, speed) * linearSpeed;
        rb.AddRelativeTorque(torque);
        rb.AddRelativeForce(force);
    }
}
