﻿using System.Collections.Generic;
using UnityEngine;

public class HintArrow : MonoBehaviour
{
    private readonly List<GameObject> coins = new List<GameObject>(); // Use a list so we can remove collected coins as we go, saving some cycles in Update()
    private Transform finishLine;
    private Transform player;
    // Start is called before the first frame update
    void Start()
    {
        coins.AddRange(GameObject.FindGameObjectsWithTag("Coin"));
        finishLine = GameObject.FindGameObjectWithTag("FinishLine").transform;
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        GameObject closest = null;
        float smallestDistance = float.MaxValue;
        for (int i = coins.Count - 1; i >= 0; --i)
        {
            GameObject coin = coins[i];
            if (coin == null)
            {
                // Remove collected coins
                coins.RemoveAt(i);
                continue;
            }
            // Find coin the least distance from us
            float dist = Vector3.Distance(player.position, coin.transform.position);
            if (dist < smallestDistance)
            {
                closest = coin;
                smallestDistance = dist;
            }
        }

        if (closest != null)
        {
            // Point arrow towards it
            transform.LookAt(closest.transform);
        }
        else
        {
            // Everything collected - point at finish line instead
            transform.LookAt(finishLine);
        }
    }
}
