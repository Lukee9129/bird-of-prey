﻿using UnityEngine;

public class FinishLine : MonoBehaviour
{
    private SceneController sceneController;

    // Start is called before the first frame update
    void Start()
    {
        sceneController = GameObject.FindGameObjectWithTag("SceneManager").GetComponent<SceneController>();
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Player") && ScoreCounter.Score > 0) // If colliding with the player and score > 0
        {
            sceneController.GameWin(); // Conclude the game (win)
        }
    }
}
