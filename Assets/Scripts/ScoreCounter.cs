﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreCounter : MonoBehaviour
{
    public static int Score = 0; // Public static so can be accessed in game win screen.
    public const int MaxScore = 10;
    private Text text;
    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
        Score = 0; // Reset score since static
    }

    public void IncrementScore()
    {
        text.text = $"Score: {++Score}/{MaxScore}"; // Increment and update displayed score
    }
}
