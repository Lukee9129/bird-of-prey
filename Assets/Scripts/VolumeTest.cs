﻿using UnityEngine;

public class VolumeTest : MonoBehaviour
{
    private AudioSource testSfx;
    // Start is called before the first frame update
    void Start()
    {
        testSfx = GetComponent<AudioSource>();
    }

    public void PlayVolume()
    {
        // Play the volume test at the set volume when slider is released.
        testSfx.volume = SceneController.Volume;
        testSfx.Play();
    }
}
