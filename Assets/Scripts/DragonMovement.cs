﻿using UnityEngine;
using UnityEngine.AI;

public class DragonMovement : MonoBehaviour
{
    private const float PlayerCatchThreshold = 10f;
    private const float MovementThreshold = 1f;
    private const float AnimationOffset = 7.5f;

    private NavMeshAgent agent;
    private Transform player;
    private Rigidbody rb;
    private SceneController controller;
    private float angularSpeed = 20f;
    private float linearSpeed = 25f;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        agent = GetComponent<NavMeshAgent>();
        rb = GetComponent<Rigidbody>();
        controller = GameObject.FindGameObjectWithTag("SceneManager").GetComponent<SceneController>();
        // Set drag and angular drag (physics)
        rb.drag = 1f;
        rb.angularDrag = 2f;
        // We want the pathfinding that NavMeshAgent provides, but we will be dealing with the movement ourselves.
        agent.updatePosition = false;
        agent.updateRotation = false;
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(transform.position, player.position);
        // If dragon is within a certain distance from the player, game over.
        if (distance <= PlayerCatchThreshold)
            controller.GameOver();
    }

    // Physics updates
    void FixedUpdate()
    {
        Vector3 nextDestination;
        if (player.position.y >= 75)
        {
            // If the player is above the tallest building, do not use path finding, just fly directly towards the player.
            nextDestination = player.position;
        }
        else
        {
            // Otherwise, use the NavMesh to try and calculate a path to the player taking into account buildings.
            NavMeshPath pathToPlayer = new NavMeshPath();
            Vector3 playerFloor = player.position;
            playerFloor.y = 0; // NavMesh doesn't really work for flying agents normally, so pretend the player is always on the floor.
            agent.CalculatePath(playerFloor, pathToPlayer);
            Vector3[] pathCorners = pathToPlayer.corners;
            if (pathCorners.Length > 1) // If a valid path was calculated, set the next available node as the destination.
            {
                nextDestination = pathCorners[1];
                nextDestination.y = player.position.y; // Set the destination y value to the same as the (flying) player, instead of on the floor.
            }
            else
            {
                return; // No valid path was found.
            }
        }

        Vector3 dragonPosition = transform.position;
        dragonPosition.y += AnimationOffset; // Account for the dragon animation being -7.5f from the GameObject's position.
        Vector3 difference = (nextDestination - dragonPosition).normalized;
        float yDif = difference.y; // Save the difference in y coordinate between the player and dragon.
        difference.y = 0;

        // Calculate the horizontal angle between the dragon and the player.
        float angle = Vector3.SignedAngle(transform.forward, difference, Vector3.up);
        Vector3 angles = new Vector3(0, angle, 0);
        // Start rotating by that angle so that the dragon is facing the player.
        Quaternion deltaRotation = Quaternion.Euler(angles * angularSpeed * Time.deltaTime);
        rb.MoveRotation(rb.rotation * deltaRotation);
        
        Vector3 force = new Vector3(0, yDif, 0); // Always apply the difference in y as a force so the dragon will attempt to match the player's altitude.
        if (Mathf.Abs(angle) < MovementThreshold)
            force += Vector3.forward; // If the dragon is facing the bird, move forward. This makes the dragon slow down when it needs to rotate.
        rb.AddRelativeForce(force * linearSpeed);

        agent.nextPosition = rb.position; // Manually update the dragon's position for the path finder.
    }
}
