﻿using UnityEngine;

public class BirdsEyeCamera : MonoBehaviour
{
    private GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        // Follow the player +50 above their head
        Vector3 playerPos = player.transform.position;
        Vector3 playerAngles = player.transform.eulerAngles;
        transform.position = new Vector3(playerPos.x, playerPos.y + 50, playerPos.z);
        transform.rotation = Quaternion.Euler(transform.eulerAngles.x, playerAngles.y, playerAngles.z);
    }
}
